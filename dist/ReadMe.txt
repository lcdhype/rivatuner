LCDHype RivaTuner plugin v1.1

This plugin allows importing any hardware monitoring statistics from RivaTuner
into LCDHype. RivaTuner must be loaded and its hardware monitoring module must
be active in order to get output from this plugin.

The plugin accepts the following parameters:

- Explicitly specified name of a data source to be imported from RivaTuner. For
  example, %UsePlugin('rivatuner\rivatuner.dll','Core temperature') will return
  current value retreived from the data source named 'Core temeprature' if it's
  available.	
- All avaialble data sources can be dynamically formatted by the plugin using @
  parameter with the following syntax:

  @,[x0,x1,x2,y0,dY]

  where [X0:Y0] is origin of the first column displaying data source name (e.g.
  CPU Clock) [X1:Y0] is the origin of the second column displayig current value
  retreived from the data source (e.g. 2500) [X2:Y0] is the origin of the third
  column displaying current data source dimension (e.g. MHz) and dY is a delta
  to be added to Y0 for each new data source. 
  For example %UsePlugin('rivatuner\rivatuner.dll',@,5,160,210,5,10) suits for
  displaying all available data sources using the following tabbing:  

  [5:5 ]Name1 [160:5 ]: Value1 [210:5 ]Dimension1
  [5:15]Name2 [160:15]: Value2 [210:15]Dimension2
  ...
  [5:55]NameN [160:55]: ValueN [210:55]DimensionN

(C) 2007 by Alexey Nicolaychuk aka Unwinder