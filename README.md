# RivaTuner plugin for LCDHype

This plugin allows importing any hardware monitoring statistics from [RivaTuner](https://nvworld.ru/utilities/rivatuner) 2.24 or above
into LCDHype.

Currently the folders of this repository are simply mirroring the following sources:

Folder | Source
--- | ---
`dist` | `%RivaTuner%\SDK\Samples\Plugins\LCDHype\RivaTuner\Release`
`src` | `%RivaTuner%\SDK\Samples\Plugins\LCDHype\RivaTuner`